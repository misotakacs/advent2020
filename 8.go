package main

import (
"bufio"
"os"
"fmt"
//"regexp"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}


func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        fmt.Printf("Failed to parse strings %s: %v\n")
        os.Exit(1)
    }
    return i
}

type bag struct {
    color string
    num int
}

type instr string

const (
    nop instr = "nop"
    acc instr = "acc"
    jmp instr = "jmp"
)


type code struct {
    i instr
    val int
    executed bool
}
func run(is []*code) (bool, int) {
    ip := 0
    accVal := 0
    for _, i := range is { 
        i.executed = false
    }
    for {
        fmt.Printf("code: %v\n", *is[ip])
        if is[ip].executed {
            fmt.Printf("acc=%d\n", accVal)
            return false, 0
        }
        is[ip].executed = true
        switch is[ip].i {
            case nop:
                ip++
            case acc:
                accVal += is[ip].val
                fmt.Printf("acc = acc + %d\n", is[ip].val)
                ip++
            case jmp:
                fmt.Printf("Jumping to %d\n", ip + is[ip].val)
                ip += is[ip].val
        }
        if ip == len(is) {
            fmt.Printf("Finished.\n")
            return true, accVal
        }
    }
}

func main() {
    lines := readLines()

    is := []*code{}
    for _, i := range lines {
        ps := strings.Split(i, " ")
        if len(ps) != 2 {
            fmt.Printf("Failed to parse %s\n", i)
            os.Exit(1)
        }
        is = append(is, &code{
            i: instr(ps[0]),
            val: atoi(ps[1]),
        })
    }

    for _, i := range is {
        if i.i == nop {
            i.i = jmp
            if finished, acc := run(is); finished {
                fmt.Printf("Finished with %d\n", acc)
                break
            }
            i.i = nop
        } else if i.i == jmp {
            i.i = nop
            if finished, acc := run(is); finished {
                fmt.Printf("Finished with %d\n", acc)
                break
            }
            i.i = jmp
        }
    }
}
