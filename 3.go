package main

import (
"bufio"
"os"
"fmt"
"flag"
//"strconv"
//"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}


var (
  dx = flag.Int("dx", 1, "dx")
  dy = flag.Int("dy", 1, "dy")
  )

func main() {
    i := readLines()
    flag.Parse()

    x := 0
    y := 0
    t := 0
    for y < len(i) - *dy {
        x += *dx
        y += *dy
        if i[y][x % len(i[0])] == '#' {
            t++
        }
    }
    fmt.Printf("%d\n", t)
}
