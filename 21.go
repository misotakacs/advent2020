package main

import (
"bufio"
"os"
"fmt"
"regexp"
"sort"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type bitmap [4]uint64

func (b *bitmap) set(ix int) {
    i := ix / 64
    bt := uint(ix % 64)
    mask := uint64(1) << bt
    b[i] = b[i] | mask
}

func (b *bitmap) and(b2 *bitmap) {
    for ix := range b {
        b[ix] = b[ix] & b2[ix]
    }
}

func (b *bitmap) or(b2 *bitmap) {
    for ix := range b {
        b[ix] = b[ix] | b2[ix]
    }
}

func (b *bitmap) copy() *bitmap {
    nb := &bitmap{}
    for ix, b := range b {
        nb[ix] = b
    }
    return nb
}

func (b *bitmap) inv() *bitmap {
    nb := &bitmap{}
    for ix, b := range b {
        nb[ix] = ^b
    }
    return nb
}

func (b *bitmap) setIndexes() []int {
    is := []int{}
    for ix := 0; ix < len(b); ix++ {
        mask := uint64(1)
        for j := 0; j < 64; j++ {
            if b[ix] & mask > 0 {
                is = append(is, ix*64 + j)
            }
            mask = mask << 1
        }
    }
    return is
}

func main() {
    lines := readLines()

    r := regexp.MustCompile(`([a-z ]+) \(contains ([a-z, ]+)\)`)
    names := map[string]int{}
    inames := map[int]string{}
    nix := 0
    als := map[string]*bitmap{}
    cnts := map[int]int{}
    for _, l := range lines {
        if m := r.FindStringSubmatch(l); len(m) != 0 {
            is := strings.Split(m[1], " ")
            as := strings.Split(m[2], ", ")
            b := &bitmap{}
            for _, i := range is {
                if _, ok := names[i]; !ok {
                    names[i] = nix
                    inames[nix] = i
                    nix++
                }
                cnts[names[i]]++
                b.set(names[i])
            }
            for _, a := range as {
                if al, ok := als[a]; ok {
                    al.and(b)
                    als[a] = al
                } else {
                    als[a] = b.copy()
                }
            }
        } else {
            fmt.Printf("Failed to parse %s\n", l)
            os.Exit(1)
        }
    }

    aais := &bitmap{}
    for _, is := range als {
        ns := []string{}
        for _, nix := range is.setIndexes() {
            ns = append(ns, inames[nix])
        }
        aais.or(is)
    }
    ixs := aais.setIndexes()
    aimap := map[int]bool{}
    cnt := 0
    for _, ix := range ixs {
        aimap[ix] = true
    }
    for _, ix := range names {
        if !aimap[ix] {
            cnt += cnts[ix]
        }
    }
    fmt.Printf("Num non-allergenic: %d\n", cnt)
    solved := map[string]string{}
    for len(als) > 0 {
        var nu *bitmap
        for a, b := range als {
            ixs := b.setIndexes()
            if len(ixs) == 1 {
                nu = b.inv()
                solved[a] = inames[ixs[0]]
                delete(als, a)
                break
            }
        }
        for _, b := range als {
            b.and(nu)
        }
    }
    ak := []string{}
    for a := range solved {
        ak = append(ak, a)
    }
    sort.Strings(ak)
    ps := []string{}
    for _, a := range ak {
        ps = append(ps, solved[a])
    }
    fmt.Printf("%s\n", strings.Join(ps, ","))
}
