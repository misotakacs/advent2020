package main

import (
"bufio"
"os"
"fmt"
"strconv"
"strings"
"regexp"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func csvToInt(s string) []int {
    ms := strings.Split(s, ",")
    r := []int{}
    for _, m := range ms {
        r = append(r, atoi(m))
    }
    return r
}

type matcher struct {
    from int
    to int
}

type rule struct{
    r1 matcher
    r2 matcher
    name string
}

func (r rule) match(a int) bool {
    return r.r1.match(a) || r.r2.match(a)
}

func (m matcher) match(a int) bool {
    return a >= m.from && a <= m.to
}

type field struct {
    ix int
    fields map[string]bool
}

func main() {
    lines := readLines()

    state := 0
    matchRe := regexp.MustCompile(`(.*): ([0-9]+)-([0-9]+) or ([0-9]+)-([0-9]+)`)
    rules := map[string]rule{}
    ticket := []int{}
    nearby := [][]int{}
    for _, l := range lines {
        if len(l) == 0 {
            continue
        }
        if l == "your ticket:" {
            state = 1
            continue
        }
        if l == "nearby tickets:" {
            state = 2
            continue
        }
        if state == 0 {
            m := matchRe.FindStringSubmatch(l)
            if len(m) != 6 {
                fmt.Printf("Failed to parse %s\n", l)
                os.Exit(1)
            }
            rules[m[1]] = rule{
                name: m[1],
                r1: matcher{
                    from:atoi(m[2]),
                    to:atoi(m[3]),
                    },
                r2: matcher{
                    from:atoi(m[4]),
                    to:atoi(m[5]),
                },
            }
        }
        if state == 1 {
            ticket = csvToInt(l)
        }
        if state == 2 {
            nearby = append(nearby, csvToInt(l))
        }
    }
    sum := 0
    // Maps a position to possible rules.
    possible := map[int]map[string]bool{}
    // Initialize the maps.
    for ix := range ticket {
        possible[ix] = map[string]bool{}
    }
    valid := [][]int{}
    // I'll optimize in part2. Or not.
    for _, t := range nearby {
        tvalid := true
        for _, v := range t {
            found := false
            for _, r := range rules {
                if r.match(v) {
                    found = true
                    break
                }
            }
            if !found {
                sum += v
                tvalid = false
            }
        }
        if tvalid {
            valid = append(valid, t)
        }
    }
    fmt.Printf("sum: %d\n", sum)
    // Part2.
    for _, t := range valid {
        fcnt := 0
        for ix, v := range t {
            field := false
            for n, r := range rules {
                _, ok := possible[ix][n]
                if r.match(v) {
                    field = true
                    // If the value is there, we don't want
                    // to change it to true. Some previous ticket invalidated
                    // this position for the field.
                    if !ok {
                        possible[ix][r.name] = true
                    }
                } else {
                    // But we'll gladly change it to false...any time :)
                    possible[ix][r.name] = false
                }
            }
            if field {
                fcnt++
            }

        }
    }

    byLen := map[int]field{}

    for ix, pos := range possible {
        ps := []string{}
        for s, p := range pos {
            if p {
                ps = append(ps, s)
            }
        }
        f := field{
            ix:ix,
            fields:map[string]bool{},
        }
        for _, s := range ps {
            f.fields[s] = true
        }
        byLen[len(ps)] = f
    }
    l := 1
    prod := 1
    known := []string{}
    for l < len(byLen){
        f := byLen[l]
        for _, k := range known {
            if f.fields[k] {
                delete(f.fields, k)
            }
        }
        if len(f.fields) != 1 {
            fmt.Printf("For field %d, there is more than 1 results: %v\n", f.ix, f.fields)
            os.Exit(1)
        }
        k := []string{}
        for f := range f.fields {
            k = append(k, f)
        }
        known = append(known, k[0])
        if strings.HasPrefix(k[0], "departure") {
            prod *= ticket[f.ix]
        }
        l++
    }
    fmt.Printf("prod: %d\n", prod)
}
