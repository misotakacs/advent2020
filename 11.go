package main

import (
"bufio"
"os"
"fmt"
"flag"
"image"
"image/color"
"image/gif"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type delta struct {
    x int
    y int
}

const (
    full = '#'
    empty = 'L'
    floor = '.'
    )

var (
    ds = [8]delta{
        delta{-1,-1},
        delta{0, -1},
        delta{1,-1},
        delta{-1,0},
        delta{1,0},
        delta{-1,1},
        delta{0,1},
        delta{1,1},
    }
    )

func occ(s []string, px int, py int, maxd int) int {
    cnt := 0
    for _, d := range ds {
        for m := 1; m <= maxd; m++ {
            x := px + d.x*m
            y := py + d.y*m

            if x < 0 || y < 0 || y >= len(s) || x >= len(s[0]) {
                break
            }
            if s[y][x] == full {
                cnt++
                break
            }
            if s[y][x] == empty {
                break
            }
        }
    }
    return cnt
}

func step(s []string, maxd, maxocc int) ([]string, int, int) {
    ns := []string{}
    changes := 0
    o := 0
    for y := 0; y < len(s); y++ {
        ns = append(ns, "")
        for x := 0; x < len(s[y]); x++ {
            if rune(s[y][x]) == floor {
                ns[y] += string(floor)
            } else if rune(s[y][x]) == empty && occ(s, x, y, maxd) == 0 {
                changes++
                ns[y] += string(full)
                o++
            } else if rune(s[y][x]) == full && occ(s, x, y, maxd) >= maxocc {
                changes++
                ns[y] += string(empty)
            } else {
                if rune(s[y][x]) == full {
                    o++
                }
                ns[y] += string(s[y][x])
            }
        }
    }
    return ns, changes, o
}

func prn(s []string) {
    for _, l := range s {
        fmt.Printf("%s\n", string(l))
    }
}

var (
   palette = []color.Color{color.Black, color.White, color.RGBA{0xff,0,0,0xff}}
)

func frame(s []string) *image.Paletted {
   rect := image.Rect(0, 0, len(s[0]), len(s))
   img := image.NewPaletted(rect, palette)

   for y, l := range s {
       for x, r := range l {
           ci := uint8(0)
           switch rune(r) {
               case '#': ci = 2
               case 'L': ci = 1
           }
           img.SetColorIndex(x, y, ci)
       }
   }
   return img
}


var (
    maxd = flag.Int("maxd", 1, "maximum distance to which to see")
    maxocc = flag.Int("maxocc", 4, "maximum tolerable occupancy")
    imgf = flag.String("imgf", "anim.gif", "image filename to output")
)

func writeGIF(fname string, frames []*image.Paletted) {
	g := gif.GIF{
        Delay: []int{},
        Image: frames,
    }

    for _ = range g.Image {
        g.Delay = append(g.Delay, 5)
    }

    f, err := os.Create(fname)
    defer f.Close()
    if err != nil {
        fmt.Printf("Failed to create a file: %v", err)
        os.Exit(1)
    }

    w := bufio.NewWriter(f)

    gif.EncodeAll(w, &g)
    w.Flush()
}

func main() {
    s := readLines()

    flag.Parse()

    prn(s)

   frames := []*image.Paletted{}
   delta := 1
   occ := 0
   for delta > 0 {
        s, delta, occ = step(s, *maxd, *maxocc)
        prn(s)
        frames = append(frames, frame(s))
   }
   fmt.Printf("occ=%d\n", occ)
   writeGIF(*imgf, frames)
}
