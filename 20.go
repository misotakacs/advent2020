package main

import (
"bufio"
"os"
"fmt"
"math"
"regexp"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type dir int

const (
    UP dir = iota
    DOWN dir = iota
    LEFT dir = iota
    RIGHT dir = iota
)

var (
    rot = map[dir]dir{
        UP: RIGHT,
        RIGHT: DOWN,
        DOWN: LEFT,
        LEFT: UP,
    }
    vflip = map[dir]dir{
        UP: DOWN,
        DOWN: UP,
        LEFT: LEFT,
        RIGHT: RIGHT,
    }
    hflip = map[dir]dir{
        UP: UP,
        DOWN: DOWN,
        LEFT: RIGHT,
        RIGHT: LEFT,
    }
)

type tile struct {
    id int
    d [10][10]byte
    bm map[dir]*tile
    rot int
    vf bool
    hf bool
}

// Rotate only the border map.
func (t *tile) rotate(n int) {
    t.rot += n
    for i := 0; i < n; i++ {
        nbm := map[dir]*tile{}
        for k, v := range t.bm {
            nbm[rot[k]] = v
        }
        t.bm = nbm
    }
}

// Vertical flip of the border map.
func (t *tile) vflip() {
    t.vf = true
    nbm := map[dir]*tile{}
    for k, v := range t.bm {
        nbm[vflip[k]] = v
    }
    t.bm = nbm
}

// Horizontal flip of the border map.
func (t *tile) hflip() {
    t.hf = true
    nbm := map[dir]*tile{}
    for k, v := range t.bm {
        nbm[hflip[k]] = v
    }
    t.bm = nbm
}

func (t tile) String() string {
    s := ""
    for _, l := range t.d {
       s += fmt.Sprintf("%s\n", string(l[:len(l)])) 
    }
    return s
}

// Apply all the transformations to the data.
func (t *tile) apply() {
    nd := [10][10]byte{}
    for i := 0; i < t.rot; i++ {
        for y := 0; y < 10; y++ {
            for x := 0; x < 10; x++ {
                nd[x][9-y] = t.d[y][x]
            }
        }
        t.d = nd
    }
    t.rot = 0
    if t.vf {
        for x := 0; x < 10; x++ {
            for y := 0; y < 5; y++ {
                t.d[y][x], t.d[9-y][x] = t.d[9-y][x], t.d[y][x]
            }
        }
    }
    t.vf = false
    if t.hf {
        for y := 0; y < 10; y++ {
            for x := 0; x < 5; x++ {
                t.d[y][x], t.d[y][9-x] = t.d[y][9-x], t.d[y][x]
            }
        }
    }
    t.hf = false
}

// Map of direction to the canonical border representation.
func (t *tile) borders() map[dir]string {
    bs := map[dir]string{
        UP: string(t.d[0][:]),
        DOWN: string(t.d[len(t.d)-1][:]),
    }
    l, r := "", ""
    for _, ln := range t.d {
        l += string(ln[0])
        r += string(ln[len(ln)-1])
    }
    bs[LEFT] = l
    bs[RIGHT] = r
    return bs
}

// List of all possible border representations, including reversions.
func (t *tile) borderSet() []string {
    bs := map[string]bool{}
    for _, b := range t.borders() {
        bs[b] = true
        bs[reverse(b)] = true
    }
    ks := []string{}
    for k := range bs {
        ks = append(ks, k)
    }
    return ks
}

func reverse(s string) string {
    r := []rune(s)
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return string(r)
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func numOthers(is []int, i int) int {
    cnt := 0
    for _, oi := range is {
        if oi != i {
            cnt++
        }
    }
    return cnt
}

func other(is []int, i int) int {
    ot := map[int]bool{}
    f := 0
    for _, oi := range is {
        if oi != i {
            ot[oi] = true
            f = oi
        }
    }
    if len(ot) > 1 {
        fmt.Printf("Found more than one matching tile.")
        os.Exit(1)
    }
    if len(ot) == 0 {
        return 0
    }
    return f
}

type dirmap map[dir]*tile

func (d dir) String() string {
    switch d {
        case UP: return "UP"
        case DOWN: return "DOWN"
        case LEFT: return "LEFT"
        case RIGHT: return "RIGHT"
    }
    return "WHAT?!"
}

func (m dirmap) String() string {
    a := []string{}
    for d, t := range m {
        id := 0
        if t != nil {
            id = t.id
        }
        a = append(a, fmt.Sprintf("%s: %d", d, id))
    }
    return strings.Join(a, ", ")
}

type coord struct {
    x int
    y int
}

func rotate(i [][]byte) [][]byte {
    ni := make([][]byte, len(i))
    for ix := range ni {
        ni[ix] = make([]byte, len(i))
    }
    for y := 0; y < len(i); y++ {
        for x := 0; x < len(i); x++ {
            ni[x][len(i)-y-1] = i[y][x]
        }
    }
    return ni
}

func hf(i [][]byte) {
    for y := 0; y < len(i); y++ {
        for x := 0; x < len(i)/2; x++ {
            i[y][x], i[y][len(i)-1-x] = i[y][len(i)-1-x], i[y][x]
        }
    }
}


func vf(i [][]byte) {
    for x := 0; x < len(i); x++ {
        for y := 0; y < len(i)/2; y++ {
            i[y][x], i[len(i)-1-y][x] = i[len(i)-1-y][x], i[y][x]
        }
    }
}

func copySl(i [][]byte) [][]byte {
    r := make([][]byte, len(i))
    for y := 0; y < len(i); y++ {
        r[y] = make([]byte, len(i[y]))
        copy(r[y], i[y])
    }
    return r
}

type img [][]byte

func (i img) String() string {
    str := ""
    for _, s := range i {
        str += fmt.Sprintf("%s\n", string(s[:]))
    }
    return str
}


func main() {
    lines := readLines()

    tm := regexp.MustCompile(`Tile ([0-9]+):`)

    ts := map[int]*tile{}
    var t *tile
    tix := 0
    for _, l := range lines {
        if m := tm.FindStringSubmatch(l); len(m) > 0 {
            t = &tile{
                bm: map[dir]*tile{},
            }
            t.id = atoi(m[1])
            ts[t.id] = t
            tix = 0
        } else if l != "" {
            copy(t.d[tix][:], l[:10])
            tix++
        }
    }
    
    // Build a map of borders (inluding reverse) to tiles. 
    bs := map[string][]int{}
    for id, tl := range ts {
        for _, b := range tl.borderSet() {
            bs[b] = append(bs[b], id)
        }
    }

    var c *tile

    for id, tl := range ts {
        cnt := 0
        for dir, b := range tl.borders() {
            // Find all matching tiles for the tile.
            cnt += numOthers(bs[b], id)
            // Find the actual matching tile for each side.
            tl.bm[dir] = ts[other(bs[b], id)]
        }
        // Pick a corner tile.
        if cnt == 2 && c == nil {
            c = tl
        }
    }

    g := map[coord]*tile{}
    g[coord{0,0}] = c
    // Rotate the initial corner tile.
    if c.bm[UP] == nil {
         if c.bm[RIGHT] == nil {
            c.rotate(2)
        } else {
            c.rotate(3)
        }
    } else if c.bm[RIGHT] == nil {
        c.rotate(1)
    }
    c.apply()

    x, y := 0, 1
    placed := 1

    pDir := DOWN
    nDir := UP

    side := int(math.Sqrt(float64(len(ts))))
    if side*side != len(ts) {
        fmt.Printf("Failed to find side size of %d. Got %d.\n", len(ts), side)
        os.Exit(1)
    }

    p := c

    for placed < len(ts) {
        c = p.bm[nDir]
        // First, rotate the candidate so that the correct side is
        // pointing to the "previous" tile.
        for c.bm[pDir] != p {
            fmt.Printf("Rotating %d\n", c.id)
            c.rotate(1)
        }
        c.apply()
        // If the border is reverted, flip appropriately.
        if c.borders()[pDir] == reverse(p.borders()[nDir]) {
            if pDir == DOWN {
                fmt.Printf("Hfliping\n")
                c.hflip()
            } else {
                fmt.Printf("Vflipping\n")
                c.vflip()
            }
        }
        c.apply()
        // Sanity check.
        if c.borders()[pDir] != p.borders()[nDir] {
            fmt.Printf("P: %d\n%s\n", p.id, *p)
            fmt.Printf("C: %d\n%s\n", c.id, *c)
            os.Exit(1)
        }

        // Place the tile.
        g[coord{x:x,y:y}] = c

        fmt.Printf("Placed %d\n%s\n", c.id, *c)

        placed++
        y++
        // If we reached end of column start new column. 
        if y == side {
            fmt.Printf("Moving right; %d:%d\n", x, y)
            y = 0
            x++
            // Temporarily match the left tile, not bottom.
            pDir = LEFT
            nDir = RIGHT
            // Tile to match.
            p = g[coord{x:x-1,y:0}]
        } else {
            // Matching down.
           pDir = DOWN
           nDir = UP
           // Tile to match.
           p = c
        }
    }

    // Extract image, drop borders.
    im := make([][]byte, 8*side)
    for y := 0; y < 8*side; y++ {
        im[y] = make([]byte, 8*side)
        for x := 0; x < 8*side; x++ {
            im[y][x] = g[coord{x:x / 8, y:y / 8}].d[8-(y%8)][1+x%8]
        }
    }

    monster := []string{
        "                  # ",
        "#    ##    ##    ###",
        " #  #  #  #  #  #   ",
    }

    // Monster finding function.
    hm := func(im [][]byte) int {
        numFound := 0
        for y := 0; y < len(im) - len(monster); y++ {
            for x := 0; x < len(im[0]) - len(monster[0]); x++ {
                found := true
                checkMonster:
                for my, l := range monster {
                    for mx, b := range l {
                        if b == '#' && im[y+my][x+mx] != '#' {
                            found = false
                            break checkMonster
                        }
                    }
                }
                if found {
                    for my, l := range monster {
                        for mx, b := range l {
                            if b == '#' {
                                im[y+my][x+mx] = 'O'
                            }
                        }
                    }
                    numFound++
                }
            }
        }
        return numFound
    }

    cnt := func(i [][]byte) int {
        c := 0
        for _, l := range i {
            for _, b := range l {
                if b == '#' {
                    c++
                }
            }
        }
        return c
    }

    ii := copySl(im)

    // Rotating and flipping image until monster is found.
    for i := 0; i < 3; i++ {
        ii = rotate(ii)
        if n := hm(ii); n > 0 {
            fmt.Printf("%s", img(ii))
            fmt.Printf("Found %d monsters, roughness: %d\n", n, cnt(ii))
            break
        }
        fi := copySl(ii)
        vf(fi)
        if n := hm(fi); n > 0 {
            fmt.Printf("%s", img(fi))
            fmt.Printf("Found %d monsters, roughness: %d\n", n, cnt(fi))
            break
        }
        fi = copySl(ii)
        hf(fi)
        if n := hm(fi); n > 0 {
            fmt.Printf("%s", img(fi))
            fmt.Printf("Found %d monsters, roughness: %d\n", n, cnt(fi))
            break
        }


    }
}
