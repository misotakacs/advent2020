package main

import (
"bufio"
"os"
"fmt"
"strconv"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type opcode struct {
    i byte
    val int
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type pos struct {
    x int
    y int
    dir byte
}

type coord struct {
    x int
    y int
}

var (
    dirs = map[byte]coord{
        'E': coord{1,0},
        'W': coord{-1, 0},
        'N': coord{0, -1},
        'S': coord{0, 1},
    }
    rot90 = map[byte]map[byte]byte{
        'L': {
            'N': 'W',
            'W': 'S',
            'S': 'E',
            'E': 'N',
        },
        'R': {
            'N': 'E',
            'E': 'S',
            'S': 'W',
            'W': 'N',
        },
    }
    vrot90 = map[byte]coord{
        'L': coord{1,-1},
        'R': coord{-1, 1},
    }
)

func abs(i int) int {
    if i < 0 {
        return -i
    }
    return i
}

func move2(p, wp pos, o opcode) (pos, pos) {
        switch o.i {
            case 'F':
                p.x += wp.x*o.val
                p.y += wp.y*o.val
            case 'R':
                for i := 0; i < o.val/90; i++ {
                    wp = pos{
                        x: wp.y*vrot90['R'].x,
                        y: wp.x*vrot90['R'].y,
                    }
                }
            case 'L':
                for i := 0; i < o.val/90; i++ {
                    wp = pos{
                        x: wp.y*vrot90['L'].x,
                        y: wp.x*vrot90['L'].y,
                    }
                }
        }
        if d, ok := dirs[o.i]; ok {
            wp.x += d.x*o.val
            wp.y += d.y*o.val
        }
        return p, wp

}

func move(p pos, o opcode) pos {
        switch o.i {
            case 'F':
                p.x += dirs[p.dir].x*o.val
                p.y += dirs[p.dir].y*o.val
            case 'R':
                for i := 0; i < o.val/90; i++ {
                    p.dir = rot90['R'][p.dir]
                }
            case 'L':
                for i := 0; i < o.val/90; i++ {
                    p.dir = rot90['L'][p.dir]
                }
        }
        if d, ok := dirs[o.i]; ok {
            p.x += d.x*o.val
            p.y += d.y*o.val
        }
        return p
}

func tests() {
    t := []struct{
        s pos
        o opcode
        e pos
    }{
        {
            s: pos{0, 0, 'E'},
            o: opcode{'R', 90},
            e: pos{0, 0, 'S'},
        },
        {
            s: pos{0, 0, 'N'},
            o: opcode{'F', 90},
            e: pos{0, -90, 'N'},
        },
        {
            s: pos{0, 0, 'S'},
            o: opcode{'L', 180},
            e: pos{0, 0, 'N'},
        },
        {
            s: pos{0, 0, 'W'},
            o: opcode{'F', 90},
            e: pos{-90, 0, 'W'},
        },
        {
            s: pos{0, 0, 'E'},
            o: opcode{'N', 90},
            e: pos{0, -90, 'E'},
        },        
        {
            s: pos{0, 0, 'E'},
            o: opcode{'S', 90},
            e: pos{0, 90, 'E'},
        },        
        {
            s: pos{0, 0, 'E'},
            o: opcode{'E', 90},
            e: pos{90, 0, 'E'},
        },        
        {
            s: pos{0, 0, 'E'},
            o: opcode{'E', 90},
            e: pos{90, 0, 'E'},
        },
        {
            s: pos{-1, -1, 'E'},
            o: opcode{'W', 1},
            e: pos{-2, -1, 'E'},
        },



     }

    for _, tt := range t {
        if ee := move(tt.s, tt.o); ee != tt.e {
            fmt.Printf("expected %v for %v, %v, got %v\n", tt.e, tt.s, tt.o, ee)
            os.Exit(1)
        }
    }

}

func main() {
    lines := readLines()

    tests()

    os := []opcode{}
    for _, l := range lines {
        os = append(os, opcode{
            i: l[0],
            val: atoi(l[1:]),
        })
    }

    p := pos{x:0, y:0, dir:'E'}

    for _, o := range os {
        p = move(p, o)
        fmt.Printf("%c %d: %d,%d %c\n", o.i, o.val, p.x, p.y, p.dir)
    }
    fmt.Printf("%d\n", abs(p.x)+abs(p.y))

    // Part2
    p = pos{x:0, y:0}
    wp := pos{x:10, y:-1}
    for _, o := range os {
        p, wp = move2(p, wp, o)
        fmt.Printf("%c %d: %v, %v\n", o.i, o.val, p, wp)
    }
    fmt.Printf("%d\n", abs(p.x)+abs(p.y))

}
