package main

import (
"bufio"
"os"
"fmt"
"strconv"
//"strings"
"sort"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func part2(is []int) int {
    is = append(is, is[len(is)-1] + 3)
    sum := 1
    curr := 0
    start := 0
    end := 0
    paths := map[int]int{
        curr: 1,
    }
    for start < len(is) && end < len(is) {
        // Increase the window if possible.
        fmt.Printf("S=%d, E=%d, curr=%d, %v, paths[%d]=%d\n", start, end, curr, is[start:end+1], curr, paths[curr])
        if end < len(is) - 1 && is[end+1] <= curr + 3 {
            end++
            continue
        }
        for ix := start; ix < end+1; ix++ {
            paths[is[ix]] += paths[curr]
        }
        start++
        curr = is[start-1]
    }
    return paths[is[len(is)-1]]
}

func main() {
    lines := readLines()

    is := []int{}
    for _, l := range lines {
        is = append(is, atoi(l))
    }

    sort.Ints(is)

    d1 := 0
    d3 := 0
    last := 0
    for _, i := range is {
        if i == last + 1 {
            d1++
        }
        if i == last + 3 {
            d3++
        }
        last = i
    }

    d3++

    fmt.Printf("d1=%d, d3=%d, res=%d\n", d1, d3, d1*d3)
    fmt.Printf("Ways: %d\n", part2(is))
}
