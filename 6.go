package main

import (
"bufio"
"os"
"fmt"
//"regexp"
"strconv"
//"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}


func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        fmt.Printf("Failed to parse strings %s: %v\n")
        os.Exit(1)
    }
    return i
}


func main() {
    lines := readLines()

    numPeople := 0
    cnt := 0
    cd := map[rune]int{}
    for ix, l := range lines {
        if len(l) > 0 {
            numPeople++
            for _, r := range l {
                cd[r]++
            }
        }
        if len(l) == 0 || ix == len(lines) - 1 {
            for r, c := range cd {
                if c == numPeople {
                    fmt.Printf("%c:%d\n", r, c)
                    cnt++
                }
            }
            cd = map[rune]int{}
            numPeople = 0            
            fmt.Printf("---\n")
        }
    }
    fmt.Printf("cnt = %d\n", cnt)
}
