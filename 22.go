package main

import (
"bufio"
"os"
"fmt"
"strconv"
"strings"

"queue"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func hash(p []*queue.Queue) string {
    var sb strings.Builder
    for i := 0; i < 2; i++ {
        sb.WriteRune(rune(60+i))
        sb.WriteRune('|')
        for j := 0; j < p[i].Len; j++ {
            sb.WriteRune(rune(60+p[i].Get(j)))
            sb.WriteRune(':')
        }
    }
    return sb.String()
}
func game2(p []*queue.Queue, seen map[string]int, depth int) int {
    prev := map[string]bool{}
    w := 0
    r := 0
    //fmt.Printf("d=%d\n", depth)
    for p[0].Len  > 0 && p[1].Len > 0 {
        r += 1
        if depth < 1 {
            fmt.Printf("r=%d:%d -- %d\n", depth,r, len(seen))
        }
        h := hash(p)
//        fmt.Printf("%s\n", h)
        if prev[h] {
            w = 0
        } else if p[0].Len > p[0].Peek() && p[1].Len > p[1].Peek() {
            var ok bool
            if w, ok = seen[h]; !ok {
                pc := make([]*queue.Queue, 2)
                pc[0] = p[0].Copy(1, p[0].Peek())
                pc[1] = p[1].Copy(1, p[1].Peek())
                w = game2(pc, seen, depth+1)
                seen[h] = w
            }
        } else if p[0].Peek() > p[1].Peek() {
            w = 0
        } else {
            w = 1
        }
        prev[h] = true
        p1 := p[w].Pop()
        p2 := p[w^1].Pop()
//        fmt.Printf("%d, %d; %d\n", p1, p2, w)
        p[w].Append(p1)
        p[w].Append(p2)
//        fmt.Printf("%v | %v\n", p[0].List(), p[1].List())
    }
    if depth == 0 {
        sum := 0
        q := p[w]
        for ix := q.Len-1; ix >= 0; ix-- {
            sum = sum + (q.Get(ix)*(q.Len-ix))
        }
        fmt.Printf("sum=%d\n", sum)
    }
    return w
}

func main() {
    lines := readLines()

    p := make([][]int, 2)
    ix := -1
    for _, l := range lines {
        if len(l) == 0 {
            continue
        }
        if strings.HasPrefix(l, "Player") {
            ix++
            continue
        }
        p[ix] = append(p[ix], atoi(l))
    }

    p2 := make([]*queue.Queue, 2)
    p2[0] = &queue.Queue{
        D: make([]int, len(p[0])*2),
    }
    for i:= 0; i < len(p[0]); i++ {
        p2[0].Append(p[0][i])
    }
    p2[1] = &queue.Queue{
        D: make([]int, len(p[0])*2),
    }
    for i:= 0; i < len(p[1]); i++ {
        p2[1].Append(p[1][i])
    }

    fmt.Printf("%v\n%v\n", p2[0].List(), p2[1].List())

    game2(p2, make(map[string]int), 0)

    }
