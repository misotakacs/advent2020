package main

import (
"bufio"
"os"
"flag"
"fmt"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}


func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type num struct {
    t int
    cnt int
}

var (
   target = flag.Int("target", 2020, "The target turn.")
)


func main() {
    flag.Parse()
    lines := readLines()
    nss := strings.Split(lines[0], ",")
    ns := []int{}
    for _, s := range nss {
        ns = append(ns, atoi(s))
    }

    lastturn := map[int]num{}
    for ix, n := range ns {
        lastturn[n] = num{
            t: ix+1,
            cnt: 1,
        }
    }
    last := ns[len(ns)-1]
    t := len(ns)
    for {
        t++
        // Special case for seeded numbers.
        if lastturn[last].t == t-1 && lastturn[last].cnt == 1 {
            last = 0
        } else {
            // not found
            if lastturn[last].t == 0 {
                lastturn[last] = num{t: t - 1, cnt: 1}
                last = 0
            } else {
                diff := t - lastturn[last].t - 1
                lastturn[last] = num{t: t - 1, cnt: lastturn[last].cnt+1}
                last = diff
            }
        }
        if t == *target {
            fmt.Printf("[%d] = %d\n", t, last)
            fmt.Printf("finished\n")
            break
        }
    }
}
