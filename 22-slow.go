package main

import (
"bufio"
"os"
"fmt"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func hash(p [][]int) string {
    var sb strings.Builder
    for i := 0; i < 2; i++ {
        sb.WriteRune(rune(60+i))
        sb.WriteRune('|')
        for _, j := range p[i] {
            sb.WriteRune(rune(60+j))
            sb.WriteRune(':')
        }
    }
    return sb.String()
}

func game2(p [][]int, seen map[string]int, depth int) int {
    prev := map[string]bool{}
    w := 0
//    fmt.Printf("Recursing game\n")
       r := 0
    //fmt.Printf("d=%d\n", depth)
    for len(p[0]) > 0 && len(p[1]) > 0 {
        r += 1
        if depth < 1 {
            fmt.Printf("r=%d:%d -- %d\n", depth,r, len(seen))
        }
        h := hash(p)
//        fmt.Printf("%s\n", h)
        if prev[h] {
            w = 0
        } else if len(p[0]) > p[0][0] && len(p[1]) > p[1][0] {
            var ok bool
            if w, ok = seen[h]; !ok {
                pc := make([][]int, 2)
                pc[0] = make([]int, p[0][0])
                copy(pc[0], p[0][1:])
                pc[1] = make([]int, p[1][0])
                copy(pc[1], p[1][1:])
                w = game2(pc, seen, depth+1)
                seen[h] = w
            }
        } else if p[0][0] > p[1][0] {
            w = 0
        } else {
            w = 1
        }
        prev[h] = true
        p[w] = append(p[w][1:], p[w][0], p[w^1][0])
        p[w^1] = p[w^1][1:]
    //    fmt.Printf("%v  |  %v\n", p[0], p[1])
    }
    if depth == 0 {
        sum := 0
        for ix := 1; ix <= len(p[w]); ix++ {
            sum = sum + (p[w][len(p[w]) - ix]*ix)
        }
        fmt.Printf("sum=%d\n", sum)
    }
    return w
}

func main() {
    lines := readLines()

    p := make([][]int, 2)
    ix := -1
    for _, l := range lines {
        if len(l) == 0 {
            continue
        }
        if strings.HasPrefix(l, "Player") {
            ix++
            continue
        }
        p[ix] = append(p[ix], atoi(l))
    }

    p2 := make([][]int, 2)
    p2[0] = make([]int, len(p[0]))
    copy(p2[0], p[0])
    p2[1] = make([]int, len(p[1]))
    copy(p2[1], p[1])
/*
    w := 0
    for len(p[0]) > 0 && len(p[1]) > 0 {
        if p[0][0] > p[1][0] {
            w = 0
        } else {
            w = 1
        }
        p[w] = append(p[w][1:], p[w][0], p[w^1][0])
        p[w^1] = p[w^1][1:]
        fmt.Printf("%v\n", p)
    }
    sum := 0
    for ix := 1; ix <= len(p[w]); ix++ {
        sum = sum + (p[w][len(p[w]) - ix]*ix)
    }
    fmt.Printf("sum=%d\n", sum)
*/
    game2(p2, make(map[string]int), 0)
}
