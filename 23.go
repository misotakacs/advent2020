package main

import (
"bufio"
"os"
"fmt"
"strconv"
//"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type node struct {
    next *node
    val int
}

func main() {
    lines := readLines()

    m := make(map[int]*node)
    var first, prev *node 
    max := 0
    for _, c := range(lines[0]) {
        n := &node{
            val: int(c - '0'),
        }
        if n.val > max {
            max = n.val
        }
        if first == nil {
            first = n
        }
        if prev != nil {
            prev.next = n
        }
        m[n.val] = n
        prev = n
    }
    for i := max+1; i <= int(1e6); i++ {
        n := &node{
            val: i,
        }
        prev.next = n
        m[n.val] = n
        prev = n
    }
    prev.next = first
    max = int(1e6)

    curr := first
    for i := 0; i < int(1e7); i++ {
        start := curr.next
        end := curr.next.next.next

        // close circle
        curr.next = end.next

        var dest *node
        dest_label := curr.val
        for {
            dest_label -= 1
            if dest_label < 1 {
                dest_label = max
            }
            dest = m[dest_label]
            if dest != start && dest != start.next && dest != start.next.next {
                break
            }
        }
        end.next = dest.next
        dest.next = start
        curr = curr.next
    }

    curr = m[1]

    fmt.Printf("%d * %d = %d\n", curr.next.val, curr.next.next.val, int64(curr.next.val) * int64(curr.next.next.val))
/*
    str := ""
    for curr.next.val != 1 {
        str += fmt.Sprintf("%d", curr.next.val)
        curr = curr.next
    }

    fmt.Printf("%s\n", str)
*/    
}

