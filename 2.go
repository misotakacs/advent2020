package main

import (
"bufio"
"os"
"fmt"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type pw struct {
    letter rune
    min int
    max int
    pass string
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func main() {
    lines := readLines()


    valid := 0
    valid2 := 0

    for _, l := range lines {
        ps := strings.Split(l, ": ")
        p := pw{
            pass: ps[1],
        }
        pps := strings.Split(ps[0], " ")
        mm := strings.Split(pps[0], "-")
        p.min = atoi(mm[0])
        p.max = atoi(mm[1])
        p.letter = rune(pps[1][0])


        cnt := 0
        for _, r := range p.pass {
            if r == p.letter {
                cnt++
            }
        }

        if (rune(p.pass[p.min-1]) == p.letter && rune(p.pass[p.max-1]) != p.letter) ||
           (rune(p.pass[p.min-1]) != p.letter && rune(p.pass[p.max-1]) == p.letter) {
               valid2++
           }

        fmt.Printf("%c %v %d\n", p.letter, p, cnt)

        if cnt >= p.min && cnt <= p.max {
            fmt.Printf("valid\n")
            valid++
        }
    }

    fmt.Printf("valid: %d, %d\n", valid, valid2)
}
