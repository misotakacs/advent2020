$count = 0;
$max = 0;

my %seats=();

foreach $line (<STDIN>)  {   
    $line =~ s/F/0/g;
    $line =~ s/B/1/g;
    $line =~ s/R/1/g;
    $line =~ s/L/0/g;
    $num = eval("0b$line");
    print "$num\n";
    if ($num > $max) {
        $max = $num;
    }
    $seats{$num} = 1;
}

foreach $key (keys %seats) {
    if ($seats{$key+1} != 1 && $seats{$key+2} == 1) {
        $missing = $key+1;
        print "missing: $missing \n";
    }
}

print "max: $max \n";
