package main

import (
"bufio"
"os"
"fmt"
"regexp"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}


func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        fmt.Printf("Failed to parse strings %s: %v\n")
        os.Exit(1)
    }
    return i
}

type bag struct {
    color string
    num int
}

func main() {
    lines := readLines()

    cd := map[string][]string{}
    contained := map[string][]bag{}
    reC := regexp.MustCompile("(\\d)+ (.*) bag")
    re := regexp.MustCompile("^(.*) bags contain (.*)\\.$")
    for _, l := range lines {
        m := re.FindStringSubmatch(l)
        if len(m) != 3 {
            fmt.Printf("Failed to parse %s, got %d groups\n", l, len(m))
            os.Exit(1)
        }
        if m[2] == "no other bags" {
            continue
        }
        container := m[1]
        ps := strings.Split(m[2], ", ")
        for _, p := range ps {
            cs := reC.FindStringSubmatch(p)
            if len(cs) != 3 {
                fmt.Printf("Failed to parse %s, got %d groups\n", p, len(cs))
            }
            fmt.Printf("Adding %s,%d to containers of %s\n", container, atoi(cs[1]), cs[2])
            // a color points to all of its direct containers
            cd[cs[2]] = append(cd[cs[2]], container)
            contained[container] = append(contained[container], bag{
                    color: cs[2],
                    num: atoi(cs[1]),
                })
        }
    }

    start := "shiny gold"
    q := []string{start}
    holders := map[string]bool{}
    for len(q) > 0 {
        curr := q[0]
        q = q[1:]
        if curr != start {
            holders[curr] = true
        }
        q = append(q, cd[curr]...)
    }
    fmt.Printf("num different bags holding %s: %d\n", start, len(holders))

    var numBags func(string) int

    numBags = func(color string) int {
        if _, ok := contained[color]; !ok {
            return 1
        }
        cnt := 1
        for _, bg := range contained[color] {
            cnt += bg.num*numBags(bg.color)
        }
        fmt.Printf("%s: %d\n", color, cnt)
        return cnt
    }
    cnt := numBags(start)
    fmt.Printf("num contained bags in %s: %d\n", start, cnt)   
}
