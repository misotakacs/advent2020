package main

import (
"bufio"
"os"
"fmt"
"sort"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type delta struct{
    delta int
    num int
}

type deltalist []delta

func (ds deltalist) Len() int {
    return len(ds)
}

func (d deltalist) Swap(i, j int) {
    d[i], d[j] = d[j], d[i]
}

func (d deltalist) Less(i, j int) bool {
    return d[i].num < d[j].num
}

func main() {
    l := readLines()
    bs := []int{}
    for _, b := range strings.Split(l[1], ",") {
        if b == "x" {
            bs = append(bs, 0)
        } else {
            bs = append(bs, atoi(b))
        }
    }

    ds := []delta{}
    for ix, b := range bs {
        if b > 0 {
            ds = append(ds, delta{delta:ix, num:b}) 
        }
    }
    it := int64(1)
    first := ds[0]
    ds = ds[1:]
    sort.Sort(deltalist(ds))
    fmt.Printf("%v\n", ds)

    t0 := int64(0)
    dt := int64(first.num)
    newT0 := int64(-1)
    ix := 0
    for {
        busTime := t0 + it*dt
        if (busTime+int64(ds[ix].delta)) % int64(ds[ix].num) == 0 {
            found := true
            for nix := ix + 1; nix < len(ds); ix++ {
                if busTime+int64(ds[ix].delta) % int64(ds[ix].num) != 0 {
                    found = false
                    break
                }
            }
            if found {
                fmt.Printf("Found: busTime=%d\n", busTime)
                break
            }
            if newT0  < 0 {
                newT0 = busTime
            } else {
                dt = busTime - newT0
                t0 = newT0
                ix++
                newT0 = -1
                it = 0
            }
        }
        it++
    }
}
