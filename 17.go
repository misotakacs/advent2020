package main

import (
"bufio"
"os"
"fmt"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type coord struct {
    x int
    y int
    z int
}

func genNeighbors() []coord {
    c := []coord{}
    for i := -1; i <= 1; i++ {
        for j := -1; j <= 1; j++ {
            for k := -1; k <= 1; k++ {
                if k != 0 || j != 0 || i != 0 {
                    c = append(c, coord{x:i,y:j,z:k})
                }
            }
        }
    }
    return c
}

var (
    neighbors = genNeighbors()
)

// Updates state and returns number of active.
func step(cube map[coord]bool, minC, maxC coord) (map[coord]bool, int) {
    // Deltas.
    ns := neighbors
    nc := map[coord]bool{}
    active := 0
    for x := minC.x - 1; x <= maxC.x + 1; x++ {
        for y := minC.y - 1; y <= maxC.y + 1; y++ {
            for z := minC.z - 1; z <= maxC.z + 1; z++ {
                c := coord{x:x,y:y,z:z}
                cnt := 0
                for _, n := range ns {
                    if cube[coord{x+n.x, y+n.y, z+n.z}] {
                        cnt++
                    }
                }
                me := cube[c]
                fmt.Printf("%v: %t, %d\n", c, me, cnt)
                if cnt == 3 || (me && cnt == 2) {
                    nc[c] = true
                    active++
                }
            }
        }
    }
    return nc, active
}

func main() {
    lines := readLines()
    // Sparse space, we'll see if it's enough. 
    cube := map[coord]bool{}
    // Track the growth.
    minC := coord{}
    maxC := coord{}
    // Seed.
    for x, l := range lines {
        for y, c := range l {
            if c == '#' {
                cube[coord{x:x,y:y,z:0}] = true
            }
            if y > maxC.y {
                maxC.y = y
            }
        }
        if x > maxC.x {
            maxC.x = x
        }
    }

    numSteps := 6
    active := 0
    for i := 0; i < numSteps; i++ {
        cube, active = step(cube, minC, maxC)
        // Grow.
        minC.x--
        minC.y--
        minC.z--
        maxC.x++
        maxC.y++
        maxC.z++
    }
    fmt.Printf("active after %d steps: %d\n", numSteps, active)
}
