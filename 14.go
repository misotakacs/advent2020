package main

import (
"bufio"
"os"
"fmt"
"regexp"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func atoiDec(s string) int64 {
    i, err := strconv.ParseInt(s, 10, 64)
    if err != nil {
        fmt.Printf("Failed to parse %s as int: %v\n", s, err)
        os.Exit(1)
    }
    return i

}

func atoiBin(s string) int64 {
    i, err := strconv.ParseInt(s, 2, 64)
    if err != nil {
        fmt.Printf("Failed to parse %s as binary int: %v\n", s, err)
        os.Exit(1)
    }
    return i
}

func part1(lines []string) {
    mem := map[int64]int64{}
    orMask := int64(0)
    andMask := int64(0)

    for _, l := range lines {
        if m := memre.FindStringSubmatch(l); len(m) > 0 {
            mem[atoiDec(m[1])] = ( atoiDec(m[2]) | orMask ) & andMask
        } else if m := maskre.FindStringSubmatch(l); len(m) > 0 {
            orMask = atoiBin(strings.Replace(m[1], "X", "0", -1))
            andMask = atoiBin(strings.Replace(m[1], "X", "1", -1))
        }
    }

    sum := int64(0)
    for _, val := range mem {
        sum += val
    }

    fmt.Printf("sum = %d\n", sum)
}

type msk struct {
    o string
    a string
}

func masks(mask string) []msk {
    if len(mask) == 0 {
        return []msk{msk{o:"",a:""}}
    }
    m0 := mask[0]
    ms := masks(mask[1:])

    ret := []msk{}
    for _, m := range ms {
        if m0 == 'X' {
            ret = append(ret, msk{o:"0"+m.o, a:"0"+m.a})
            ret = append(ret, msk{o:"1"+m.o, a:"1"+m.a})
        } else {
            ret = append(ret, msk{o:string(m0)+m.o, a:"1"+m.a})
        }
    }
    return ret
}

type imask struct {
    o int64
    a int64
}

func part2(lines []string) {
    mem := map[int64]int64{}

    ms := []imask{}

    for _, l := range lines {
        if m := memre.FindStringSubmatch(l); len(m) > 0 {
            addr := atoiDec(m[1])
            for _, mask := range ms {
                addr = (addr & mask.a) | mask.o
                mem[addr] = atoiDec(m[2])
            }
        } else if m := maskre.FindStringSubmatch(l); len(m) > 0 {
            ms = []imask{}
            for _, m := range masks(m[1]) {
                ms = append(ms, imask{o:atoiBin(m.o),a:atoiBin(m.a)})
            }
        }
    }

    sum := int64(0)
    for _, val := range mem {
        sum += val
    }

    fmt.Printf("sum = %d\n", sum)
}

var (
    memre = regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
    maskre = regexp.MustCompile(`mask = ([01X]+)`)
)

func main() {
    lines := readLines()
    part1(lines)
    part2(lines)
}

