package main

import (
"bufio"
"os"
"fmt"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func atoi64(s string) int64 {
    i, err := strconv.ParseInt(s, 10, 64)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

func eval(s string) int {
    q := []string{}
    tok := ""
    d := 0
    for ix, b := range s {
        switch b {
        case ' ':
            if d == 0 {
                q = append(q, tok)
                tok = ""
            } else {
                tok = tok + string(b)
            }
            continue
        case '(':
            d++
            if d > 1 {
                tok = tok + string(b)
            }
            continue
        case ')':
            d--
            if d > 0 {
                tok = tok + string(b)
            }
            if d < 0 {
                fmt.Printf("Failed to parse %s at %d\n", s, ix)
                os.Exit(1)
            }
        default:
            tok = tok + string(b)
        }
    }
    if len(tok) > 0 {
        q = append(q, tok)
    }
    ix := -1
    res := 0
    op := ""
    for ix < len(q)-1 {
        ix++
        val := 0
        if q[ix] == "+" || q[ix] == "*" {
            op = q[ix]
            continue
        } else {
            if strings.Contains(q[ix], " ") {
                val = eval(q[ix])
            } else {
                val = atoi(q[ix])
            }
        }
        if op == "" {
            res = val
        } else {
            switch op {
            case "+": res += val
            case "*": res *= val
            }
        }
    }    
    return res
}

func eval2(s string) int64 {
    if !strings.Contains(s, " ") {
        return atoi64(s)
    }
    q := []string{}
    tok := ""
    d := 0
    for ix, b := range s {
        switch b {
        case ' ':
            if d == 0 {
                q = append(q, tok)
                tok = ""
            } else {
                tok = tok + string(b)
            }
            continue
        case '(':
            d++
            if d > 1 {
                tok = tok + string(b)
            }
            continue
        case ')':
            d--
            if d > 0 {
                tok = tok + string(b)
            }
            if d < 0 {
                fmt.Printf("Failed to parse %s at %d\n", s, ix)
                os.Exit(1)
            }
        default:
            tok = tok + string(b)
        }
    }
    if len(tok) > 0 {
        q = append(q, tok)
    }
    ix := -1
    res := int64(0)
    op := ""
    // Preprocess.
    for ix < len(q) - 1 {
        ix++
        if q[ix] == "+" {
            v := eval2(q[ix-1]) + eval2(q[ix+1])
            nq := append(q[:ix-1], fmt.Sprintf("%d", v))
            nq = append(nq, q[ix+2:]...)
            q = nq
            ix--
        }
    }

    ix = -1
    for ix < len(q)-1 {
        ix++
        val := int64(0)
        if q[ix] == "+" || q[ix] == "*" {
            op = q[ix]
            continue
        } else {
            val = eval2(q[ix])
        }
        if op == "" {
            res = val
        } else {
            switch op {
            case "+": res += val
            case "*": res *= val
            }
        }
    }    
    return res
}

func main() {
    lines := readLines()
    sum := 0
    for _, l := range lines {
        val := eval(l)
        sum += val
    }
    fmt.Printf("sum=%d\n", sum)
    sum2 := int64(0)
    for _, l := range lines {
        val := eval2(l)
        sum2 += val
    }
    fmt.Printf("sum2=%d\n", sum2)

}
