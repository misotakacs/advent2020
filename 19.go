package main

import (
"bufio"
"os"
"flag"
"fmt"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

type pw struct {
    letter rune
    min int
    max int
    pass string
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

type node struct {
    c byte
    alt [][]int
}

func parseNode(s string) (int, node) {
    ps := strings.Split(s, ": ")
    if len(ps) != 2 {
      panic(fmt.Sprintf("Failed to parse %s ", ps))
    }
    ni := atoi(ps[0])
    if ps[1][0] == '"' {
        return ni, node{c:ps[1][1]}
    }
    nd := node{}
    ps = strings.Split(ps[1], " | ")
    for ix, p := range ps {
        ns := strings.Split(p, " ")
        nd.alt = append(nd.alt, nil)
        for _, n := range ns {
            nd.alt[ix] = append(nd.alt[ix], atoi(n))
        }
    }
    return ni, nd
}

type vkey struct {
    rule int
    strlen int
}

func uniq(i []int) []int {
    seen := map[int]bool{}
    u :=[]int{}
    for _, n := range i {
        if seen[n] {
            continue
        }
        seen[n] = true
        u = append(u, n)
    }
    return u
}

func valid(ns map[int]node, ni int, visited map[vkey]bool, s string) (bool, []int) {
    vk := vkey{rule:ni, strlen: len(s)}
    cleanUp := true
    defer func() {
        if cleanUp {
            delete(visited, vk)
        }
    }()
    if visited[vk] {
//        fmt.Printf("already seen %v\n", vk)
        cleanUp = false
        return false, nil
    }
    visited[vkey{rule:ni, strlen: len(s)}] = true
    n := ns[ni]
    if len(s) == 0 {
//        fmt.Printf("no more string\n")
        return false, nil
    }
    if n.c > 0 && s[0] != n.c {
//        fmt.Printf("current character: %c doesn't match %c\n", s[0], n.c)
        return false, nil
    }
    if n.c > 0 && s[0] == n.c {
//       fmt.Printf("current character: [%c]%s matches %c\n", s[0], s[1:], n.c)
       return true, []int{1}
    }

    matchedAlt := false
    matchedLens := []int{}
    for _, a := range n.alt {
        altMatchedLens := []int{0}
        matched := true
        for _, r := range a {
            newML := []int{}
            matched = false
            for _, el := range altMatchedLens {
//                fmt.Printf("Matching rule %d/%d on %s\n", r, ix+1, s[el:])
                m, ls := valid(ns, r, visited, s[el:])
//                fmt.Printf("back in %d\n", ni)
                if !m {
                    continue
                } else {
                    matched = true
                    for _, l := range ls {
                        newML = uniq(append(newML, el + l))
                    }
                }
            }
            if !matched {
                break
            }
            altMatchedLens = newML
        }
        if matched {
            matchedLens = uniq(append(matchedLens, altMatchedLens...))
            if ni == 0 {
                fullMatch := false
                for _, l := range matchedLens {
                    if l == len(s) {
                        fullMatch = true
                    }
                }
                if !fullMatch {
//                    fmt.Printf("did not exhaust string %s\n", s)
                    return false, nil
                }
            }
//            fmt.Printf("%d: matched %s with %v\n", ni, s, matchedLens)
            matchedAlt = true
        }
    }
    if matchedAlt {
        return true, matchedLens
    }
//    fmt.Printf("no rule matched\n")
    return false, nil
}

var (
    part = flag.Int("part", 1, "1 or 2")
)

func main() {
    flag.Parse()
    lines := readLines()

    nodes := map[int]node{}
    strs := []string{}
    for ix, l := range lines {
        if l == "" {
            strs = lines[ix+1:]
            break
        } else {
            if *part == 2 {
                if strings.HasPrefix(l, "8:") {
                    l = "8: 42 | 42 8"
                }
                if strings.HasPrefix(l, "11:") {
                    l = "11: 42 31 | 42 11 31"
                }

            }
            ni, n := parseNode(l)
            nodes[ni] = n
        }
    }
//    fmt.Printf("%v\n", nodes)
//    fmt.Printf("%v\n", strs)

    cnt := 0
    for _, s := range strs {
        m, _ := valid(nodes, 0, map[vkey]bool{}, s)
        if m {
            cnt++
        }
//        fmt.Printf("%s: %t\n", s, m)
    }

    fmt.Printf("cnt = %d\n", cnt)
}
