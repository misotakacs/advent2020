package main

import (
"bufio"
"os"
"fmt"
"regexp"
"strconv"
"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

var (
    required = []string{
"byr",
"iyr",
"eyr",
"hgt",
"hcl",
"ecl",
"pid",
"cid",
    }
)

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        fmt.Printf("Failed to parse strings %s: %v\n")
        os.Exit(1)
    }
    return i
}

func valid(p map[string]string) bool {
    for _, r := range required {
        if _, ok := p[r]; !ok {
            if r != "cid" {
                return false
            }
        }
    }
    fmt.Printf("Validating: %v\n", p)
    byr := atoi(p["byr"])
    if byr < 1920 || byr > 2002 {
        fmt.Printf("Bad byr\n")
        return false
    }
    iyr := atoi(p["iyr"])
    if iyr < 2010 || iyr > 2020 {
         fmt.Printf("Bad iyr\n")
       return false
    }
    eyr := atoi(p["eyr"])
    if eyr < 2020 || eyr > 2030 {
         fmt.Printf("Bad eyr\n")
       return false
    }
    re := regexp.MustCompile("^([0-9]+)(in|cm)$")
    ss := re.FindStringSubmatch(p["hgt"])
    if len(ss) != 3 {
        fmt.Printf("Can't parse height: %v\n", p["hgt"])
        return false
    }
    ht := atoi(ss[1])
    if ss[2] == "cm" && (ht < 150 || ht > 193) {
         fmt.Printf("Bad ht in cm: %d\n", ht)
       return false
    }
    if ss[2] == "in" && (ht < 59 || ht > 76) {
         fmt.Printf("Bad ht in in %d\n", ht)
       return false
    }
    re = regexp.MustCompile("^#[0-9a-f]{6}$")
    if !re.MatchString(p["hcl"]) {
         fmt.Printf("Bad hcl\n")
       return false
    }
    re = regexp.MustCompile("^(amb|blu|brn|gry|grn|hzl|oth)$")
    if !re.MatchString(p["ecl"]) {
         fmt.Printf("Bad ecl\n")
       return false
    }
    re = regexp.MustCompile("^[0-9]{9}$")
    if !re.MatchString(p["pid"]) {
         fmt.Printf("Bad pid\n")
       return false
    }
    fmt.Printf("Valid!")
    return true
}

func main() {
    lines := readLines()
    
    v := 0
    cd := map[string]string{}
    for _, l := range lines {
        if len(l) == 0 {
            vd := valid(cd)
            if vd {
                v++
            }
            fmt.Printf("[%t] %v\n", vd, cd)
           cd = map[string]string{}
            continue
        }
        ps := strings.Split(l, " ")
        for _, p := range ps {
            fs := strings.Split(p, ":")
            cd[fs[0]] = fs[1]
        }
    }
    // last entry
    if valid(cd) {
        v++
    }
    fmt.Printf("%d\n", v)
}
