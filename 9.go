package main

import (
"bufio"
"os"
"fmt"
"flag"
"strconv"
//"strings"
)

func readLines() []string {
	scanner := bufio.NewScanner(os.Stdin)
	lines := []string{}

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	return lines
}

func atoi(s string) int {
    i, err := strconv.Atoi(s)
    if err != nil {
        panic(fmt.Sprintf("%v", err))
    }
    return i
}

var (
  preamble = flag.Int("preamble", 25, "Length of the preamble.")
)

func main() {
    lines := readLines()

    flag.Parse()

    lenP := *preamble
    fmt.Printf("Preamble length: %d\n", lenP)

    preMap := map[int]bool{}
    pre := []int{}

    invalid := 0

    for _, l := range lines {
        i := atoi(l)
        if lenP > 0 {
            lenP--
            preMap[i] = true
            pre = append(pre, i)
            continue
        }
        fmt.Printf("Considering %d\n", i)
        // See if the number is valid in the preamble.
        found := false
        for n := range preMap {
            d := i - n
            if d == n {
                continue
            }
            if _, ok := preMap[d]; ok {
                delete(preMap, pre[0])
                pre = append(pre[1:], i)
                preMap[i] = true
                found = true
                break
            }
        }
        if !found {
            fmt.Printf("Number %d is not valid\n", i)
            invalid = i
            break
        }
    }

    sum := 0
    min := int(1e10)
    max := 0
    nums := []int{}
    start := 0


    for _, l := range lines {
        i := atoi(l)
        nums = append(nums, i)
        sum = sum + i
        for sum > invalid {
            sum = sum - nums[start]
            start++
        }
        end := len(nums) 
        if sum == invalid {
            for ix := start;
                ix < end;
                ix++ {
                fmt.Printf("ix=%d, val=%d\n", ix, nums[ix])
                if min > nums[ix] {
                    min = nums[ix]
                }
                if max < nums[ix] {
                    max = nums[ix]
                }
            }
            fmt.Printf("min=%d, max=%d, sum=%d\n", min, max, min+max)
           break
        }
    }

}
